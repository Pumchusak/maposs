<?php
ini_set('display_errors', 1);
error_reporting();
set_error_handler(function($errno, $errstr, $errfile, $errline ) {
    throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
});
set_time_limit(300);

$date   = new DateTime();
$result = $date->format('Y-m-d');
$krr    = explode('-', $result);
$timedatestamp = implode("", $krr);
$path = str_replace('\\','/',dirname(__FILE__)).'/'.implode("", $krr);
if (!is_dir($path)) {
	mkdir($path, 0777, true);
}

function error($err){
	$date   = new DateTime();
	$result = $date->format('Y-m-d H:i:s');
	$txt = $result.' '.$err;
	$path = str_replace('\\','/',dirname(__FILE__)).'/error.log';
	error_log($txt.PHP_EOL, 3, $path);
}

$user = '';
$password = '';
$DNS = 'QuickBooks Data 64-Bit QRemote';

try {
	$connection = odbc_connect($DNS, $user, $password);
}catch(Exception $e) {
	$err = $e->getMessage();
	error($err);
	die();
}

// Live
/*
$startDate =  date('Y-m-d', strtotime('-7 days'));
$endDate = date('Y-m-d');
$startDateTime =  date('Y-m-d 00:00:00.000', strtotime('-7 days'));
$endDateTime = date('Y-m-d 23:59:59.000');
*/

$startDate =  date('Y-m-d', strtotime('-6900 days'));
$endDate = date('Y-m-d', strtotime('+1000 days'));
$startDateTime =  date('Y-m-d 00:00:00.000', strtotime('-6500 days'));
$endDateTime = date('Y-m-d 23:59:59.000', strtotime('+1700 days'));

// 1 Distributor Account
$query = "SELECT CustomFieldMTHCustomerAC, 
				BillAddressCity, 
				BillAddressCountry, 
				BillAddressAddr2, 
				BillAddressPostalCode, 
				BillAddressProvince, 
				CustomFieldRelationshipType, 
				CustomerTypeRefFullName, 
				Fax, 
				CustomFieldMarketSegment, 
				CompanyName, 
				CustomFieldOwner, 
				TermsRefFullName, 
				IsActive, 
				Phone, 
				CurrencyRefFullName, 
				ShipAddressAddr2
		FROM Customer";
		//WHERE TimeCreated > {ts'".$startDateTime."'} AND TimeCreated <= {ts'".$endDateTime."'}";		

try {
	$result = odbc_exec($connection, $query);
	$csv = "";
	while( $row = odbc_fetch_array( $result) ) {
		
		//echo '<pre>';
		//print_r($row);
		//echo '</pre>';
		
		$BillAddressCity = (!empty($row["BillAddressCity"]))? $row["BillAddressCity"] : 'Bangkok'; 
	    $BillAddressCountry = substr($row["CustomFieldMTHCustomerAC"],0,2);
		$CustomerTypeRefFullName = substr($row["CustomerTypeRefFullName"],0,3);
		$CustomFieldMarketSegment = substr($row["CustomFieldMarketSegment"],0,3);
		$IsActive = (($row["IsActive"])==1)? 'Active' : ''; 
		$CurrencyRefFullName = (($row["CurrencyRefFullName"])=='Thai Baht')? 'THB' : 'USD'; 
		
		$csv .= $row["CustomFieldMTHCustomerAC"]
			 .",".$BillAddressCity
			 .","
			 .",".$BillAddressCountry
			 .",".'"'.$row["BillAddressAddr2"].'"'
			 .","
			 .",".$row["BillAddressPostalCode"]
			 .",,"
			 .",".$row["CustomFieldRelationshipType"]
			 .",".'="'.$CustomerTypeRefFullName.'"'
			 .",,,,,,,,,,"
			 .",".'"'.$row["Fax"].'"'
			 .",".'="'.$CustomFieldMarketSegment.'"'
			 .",".'"'.$row["CompanyName"].'"'
			 .","
			 .",".'"'.$row["CustomFieldOwner"].'"'
			 .",".'"'.$row["TermsRefFullName"].'"'
			 .","
			 .",".$IsActive
			 .",".'"'.$row["Phone"].'"'
			 .","
			 .",".'"'.$CurrencyRefFullName.'"'
			 .",,,,,,,,,,,,,"
			 .",".'"'.$row["ShipAddressAddr2"].'"'
			 .",".'"'.$row["BillAddressAddr2"].'"'
			 .","
			 ."\r\n";
	}
	
	$filename = 'Distributor Account.csv';
	$csv_handler = fopen ($path.'/'.$filename,'w');
	fwrite ($csv_handler,$csv);
	fclose ($csv_handler);
		
}catch(Exception $e) {
	$err = $e->getMessage();
	error($err);
	die();
}
echo 'Distributor Account successfully'."\r\n";

// 2 Distributor Account Summary
/*
$query = "SELECT c.CustomFieldMTHCustomerAC,
				 c.CustomFieldProductLine,
				 COUNT(i.RefNumber) AS RefNumber, 
				 SUM(i.SubTotal) AS SubTotal,
				 {d Year(i.TxnDate)} AS invYear 
		FROM Customer c 
			INNER JOIN Invoice i ON c.ListID = i.CustomerRefListID 
		GROUP BY c.ListID,i.CustomerRefListID,{d Year(i.TxnDate)},c.CustomFieldProductLine,c.CustomFieldMTHCustomerAC";		
*/

$query = "SELECT CustomFieldMTHCustomerAC, 
		  	     TimeCreated, 
				 CustomFieldProductLine, 
				 CustomFieldMarketSegment 
		 FROM Customer";
		 //WHERE TimeCreated > {ts'".$startDateTime."'} AND TimeCreated <= {ts'".$endDateTime."'}";

try {
	$result = odbc_exec($connection, $query);
	$csv = "";
	while( $row = odbc_fetch_array( $result) ) {
	
		$TimeCreated = substr($row["TimeCreated"],0,4);
		$CustomFieldMarketSegment = substr($row["CustomFieldMarketSegment"],0,3);
	
		$csv .= $row["CustomFieldMTHCustomerAC"]
			 .",".$TimeCreated
			 .",".$row["CustomFieldProductLine"]
			 .",".'="'.$CustomFieldMarketSegment.'"'
			 ."\r\n";
	}
	
	$filename = 'Distributor Account Summary.csv';
	$csv_handler = fopen ($path.'/'.$filename,'w');
	fwrite ($csv_handler,$csv);
	fclose ($csv_handler);
		
}catch(Exception $e) {
	$err = $e->getMessage();
	error($err);
	die();
}
echo 'Distributor Account Summary successfully'."\r\n";

// 3 Distributor Orders
$query = "SELECT CustomFieldMTHCustomerAC, 
				ShipDate, 
				ClassRefFullName, 
				PONumber, 
				CustomFieldOwner, 
				TermsRefFullName, 
				DueDate, 
				ShipAddressBlockAddr1, 
				ShipAddressCity,	
				ShipAddressBlockAddr2, 
				ShipAddressCounty, 
				IsFullyInvoiced, 
				SubTotal, 
				CurrencyRefFullName,  
				TimeCreated, 
				CustomFieldOrderType, 
				CustomFieldPartToBeChecked, 
				RefNumber, 
				CustomFieldMarketSegment 
		FROM SalesOrder
		WHERE TimeCreated > {ts'".$startDateTime."'} AND TimeCreated <= {ts'".$endDateTime."'}";		

try {
	$result = odbc_exec($connection, $query);
	$csv = "";
	while( $row = odbc_fetch_array( $result) ) {
		
		$ClassRefFullName = substr($row["ClassRefFullName"],0,3);
		
		if ($ClassRefFullName != 'ASS') {
		
			$ShipDate = date('d/m/Y', strtotime($row["ShipDate"]));
			$DueDate = date('d/m/Y', strtotime($row["DueDate"]));
			$TimeCreated = date('d/m/Y', strtotime($row["TimeCreated"]));
			$CustomFieldMarketSegment = substr($row["CustomFieldMarketSegment"],0,3);	
			$IsFullyInvoiced = (($row["IsFullyInvoiced"])=='0')? '020' : '050'; 
			$CurrencyRefFullName = (($row["CurrencyRefFullName"])=='Thai Baht')? 'THB' : 'USD';
			$CustomFieldPartToBeChecked = substr($row["CustomFieldPartToBeChecked"],0,3);	
			
			$csv .= $row["CustomFieldMTHCustomerAC"]
				 .",".$ShipDate
				 .",,,,"
				 .",".$row["ClassRefFullName"]
				 .",".$row["PONumber"]
				 .",".'"'.$row["CustomFieldOwner"].'"'
				 .",".'"'.$row["TermsRefFullName"].'"'
				 .","
				 .",".$DueDate
				 .",,"
				 .",".'"'.$row["ShipAddressBlockAddr1"].'"'
				 .",".'"'.$row["ShipAddressCity"].'"'
				 .",".'"'.$row["ShipAddressBlockAddr2"].'"'
				 .",".'"'.$row["ShipAddressCounty"].'"'
				 .",".'="'.$IsFullyInvoiced.'"'
				 .",".$row["SubTotal"]
				 .",".'"'.$CurrencyRefFullName.'"'
				 .","
				 .",".$ShipDate
				 .","
				 .",".$TimeCreated
				 .",".$TimeCreated
				 .","
				 .",".'"'.$row["CustomFieldOrderType"].'"'
				 .",".'="'.$CustomFieldPartToBeChecked.'"'
				 .","
				 .",".'"'.$row["RefNumber"].'"'
				 .",".$row["SubTotal"]
				 .",".'="'.$CustomFieldMarketSegment.'"'
				 ."\r\n";			 
		}	 
	}
	
	$filename = 'Distributor Orders.csv';
	$csv_handler = fopen ($path.'/'.$filename,'w');
	fwrite ($csv_handler,$csv);
	fclose ($csv_handler);
		
}catch(Exception $e) {
	$err = $e->getMessage();
	error($err);
	die();
}
echo 'Distributor Orders successfully'."\r\n";

// 4 Distributor Order Lines
$query = "SELECT ClassRefFullName, 
				Subtotal, 
				SalesOrderLineDesc, 
				SalesOrderLineSeqNo, 
				CustomFieldOwner, 
				SalesOrderLineRate, 
				SalesOrderLineItemRefFullName, 
				SalesOrderLineQuantity, 
				SalesOrderLineInvoiced, 
				DueDate, 
				PONumber, 
				SalesOrderLineUnitOfMeasure, 
				CustomFieldMTHCustomerAC, 
				TimeCreated
		FROM SalesOrderLine
		WHERE (TimeCreated > {ts'".$startDateTime."'} AND TimeCreated <= {ts'".$endDateTime."'}) AND (SalesOrderLineQuantity > 0)";		

try {
	$result = odbc_exec($connection, $query);
	$csv = "";
	while( $row = odbc_fetch_array( $result) ) {
	
		$DueDate = date('d/m/Y', strtotime($row["DueDate"]));
		$TimeCreated = date('d/m/Y', strtotime($row["TimeCreated"]));
		$SalesOrderLineQuantity = (int) $row["SalesOrderLineQuantity"];
		$ClassRefFullName = substr($row["ClassRefFullName"],0,3);
		$SalesOrderLineInvoiced = (int) $row["SalesOrderLineInvoiced"];
		$SaleInvoice = $SalesOrderLineQuantity - $SalesOrderLineInvoiced;
		
		if ($ClassRefFullName != 'ASS') {

			$csv .= $row["Subtotal"]
				 .",".'"'.$row["SalesOrderLineDesc"].'"'
				 .",".$row["SalesOrderLineSeqNo"]
				 .","
				 .",".'"'.$row["CustomFieldOwner"].'"'
				 .",".$row["SalesOrderLineRate"]
				 .","
				 .",".'"'.$row["SalesOrderLineItemRefFullName"].'"'
				 .",".$row["SalesOrderLineQuantity"]
				 .",".$SaleInvoice
				 .",".'0'
				 .",".$row["SalesOrderLineInvoiced"]
				 .",".$DueDate
				 .",,"
				 .",".$row["PONumber"]
				 .",".$row["SalesOrderLineUnitOfMeasure"]
				 .",".'each'
				 .","
				 .",".'"'.$row["CustomFieldMTHCustomerAC"].'"'
				 .",".$TimeCreated
				 .",".'"'.$row["CustomFieldMTHCustomerAC"].'"'
				 ."\r\n";	 
		}
	}
	
	$filename = 'Distributor Order Lines.csv';
	$csv_handler = fopen ($path.'/'.$filename,'w');
	fwrite ($csv_handler,$csv);
	fclose ($csv_handler);
		
}catch(Exception $e) {
	$err = $e->getMessage();
	error($err);
	die();
}
echo 'Distributor Order Lines successfully'."\r\n";

// 5 Distributor Invoices
$query = "SELECT CustomFieldMTHCustomerAC, 
				DueDate,
				RefNumber, 
				ClassRefFullName, 
				CustomFieldOwner, 
				IsPaid, 
				SubTotal, 
				SalesTaxTotal, 
				CurrencyRefFullName, 
				TxnDate 
		FROM Invoice
		WHERE TxnDate > {d'".$startDate."'} AND TxnDate <= {d'".$endDate."'}";

try {
	$result = odbc_exec($connection, $query);
	$csv = "";
	while( $row = odbc_fetch_array( $result) ) {
		
		$DueDate = date('d/m/Y', strtotime($row["DueDate"]));
		$ClassRefFullName = substr($row["ClassRefFullName"],0,3);
		$IsPaid = (($row["IsPaid"])=='0')? '020 PRINTED' : '050 CLOSED'; 
		$CurrencyRefFullName = (($row["CurrencyRefFullName"])=='Thai Baht')? 'THB' : 'USD';
		$TxnDate = date('d/m/Y', strtotime($row["TxnDate"]));
		$SubTotal = (Float) $row["SubTotal"];
		$SubTotal = number_format($SubTotal, 2, '.', '');
		$SalesTaxTotal = (Float) $row["SalesTaxTotal"];
		$SalesTaxTotal = number_format($SalesTaxTotal, 2, '.', '');
		$SaleSum = $SubTotal + $SalesTaxTotal;
		$SaleSumx = number_format($SaleSum, 2, '.', '');
		
		if ($ClassRefFullName != 'ASS') {
			
			$csv .= '"'.$row["CustomFieldMTHCustomerAC"].'"'
				 .",,,"
				 .",".$DueDate
				 .","
				 .",".'"'.$row["RefNumber"].'"'
				 .",".'"'.$row["ClassRefFullName"].'"'
				 .",".'"'.$row["CustomFieldOwner"].'"'
				 .",,,,,,,,"
				 .",".'"'.$IsPaid.'"'
				 .",".$SubTotal
				 .",,,,"
				 .",".$SalesTaxTotal
				 .",".'"'.$CurrencyRefFullName.'"'
				 .",".$SaleSumx
				 .",".$TxnDate
				 ."\r\n";
		}	 
	}
	
	$filename = 'Distributor Invoices.csv';
	$csv_handler = fopen ($path.'/'.$filename,'w');
	fwrite ($csv_handler,$csv);
	fclose ($csv_handler);
		
}catch(Exception $e) {
	$err = $e->getMessage();
	error($err);
	die();
}
echo 'Distributor Invoices successfully'."\r\n";

// 6 Distributor Invoices Lines
$query = "SELECT ClassRefFullName, 
				 RefNumber, 
				 CustomFieldOwner, 
				 InvoiceLineQuantity, 
				 PONumber, 
				 CustomFieldMTHCustomerAC, 
				 TxnDate 
		FROM InvoiceLine
		WHERE TxnDate > {d'".$startDate."'} AND TxnDate <= {d'".$endDate."'}";

try {
	$result = odbc_exec($connection, $query);
	$csv = "";
	while( $row = odbc_fetch_array( $result) ) {
	
		$ClassRefFullName = substr($row["ClassRefFullName"],0,3);
		$TxnDate = date('d/m/Y', strtotime($row["TxnDate"]));
		
		if ($ClassRefFullName != 'ASS') {
		
			$csv .= ",,,"
				 .",".'"'.$row["RefNumber"].'"'
				 .",,"
				 .",".'"'.$row["CustomFieldOwner"].'"'
				 .",,"
				 .",".'"'.$row["InvoiceLineQuantity"].'"'
				 .",,,,,,"
				 .",".'"'.$row["PONumber"].'"'
				 .",".'"'.$row["CustomFieldMTHCustomerAC"].'"'
				 .",".$TxnDate
				 ."\r\n";
		}	 
	}
	
	$filename = 'Distributor Invoices Lines.csv';
	$csv_handler = fopen ($path.'/'.$filename,'w');
	fwrite ($csv_handler,$csv);
	fclose ($csv_handler);
		
}catch(Exception $e) {
	$err = $e->getMessage();
	error($err);
	die();
}
echo 'Distributor Invoices Lines successfully'."\r\n";

odbc_close($connection);
?>